# odoo-finanzas-eticas-inventory



## Getting started

1. Download in the same directory the current repository and [odoo-provisioning](https://git.coopdevs.org/coopdevs/odoo/odoo-provisioning/odoo-provisioning)
1. Follow the instructions in `odoo-provisioning` to install all the local dependencies
1. Run the following command to provision the production environment:
```
ansible-playbook playbooks/provision.yml -i ../odoo-finanzas-eticas-inventory/inventory/hosts --limit prod --ask-vault-pass
```
